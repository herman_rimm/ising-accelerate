{-# LANGUAGE BlockArguments    #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RebindableSyntax  #-}

module IsingModel where

import Data.Array.Accelerate
import Data.Array.Accelerate.Debug.Trace

-- Turn uniformly distributed values into opposing spins.
initialFlip :: Acc (Matrix Float) -> Acc (Matrix Int)
initialFlip = map (\x -> if x < 0.5 then -1 else 1)

flipSpins :: Exp Float -> Acc (Matrix (Int, Float)) -> Acc (Matrix Int)
flipSpins curie = stencil flipSpin wrap where
  flipSpin :: Stencil3x3 (Int, Float) -> Exp Int
  flipSpin ((_, top, _), (left, center, right), (_, bottom, _)) =
    if random < acceptance then -spin else spin where
      (spin, random) = unlift center
      acceptance = exp $ (-2.0) * (1 / curie) * sum * spin' where
        spin' = toFloating spin :: Exp Float
        sum = toFloating $ fst top + fst left + fst right + fst bottom :: Exp Float
