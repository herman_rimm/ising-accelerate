{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE CPP          #-}

module Main where

import Config
import IsingModel

import Control.Monad
import Data.Array.Accelerate hiding (map, (++), take, drop)
import Data.Array.Accelerate.System.Random.MWC
#if defined(ACCELERATE_LLVM_NATIVE_BACKEND)
import Data.Array.Accelerate.LLVM.Native
#elif defined(ACCELERATE_LLVM_PTX_BACKEND)
import Data.Array.Accelerate.LLVM.PTX
#else
import Data.Array.Accelerate.Interpreter
#endif
import qualified Data.Vector.Unboxed as U
import Options.Applicative
import Prelude as P hiding (zip)

main :: IO ()
main = do
  opts <- customExecParser (prefs disambiguate) options
  gen <- case optSeed opts of
    Nothing     -> createSystemRandom
    Just seed   -> initialize (U.singleton seed)
  let size@(!x, !y) = optSize opts
  -- Create lattice with spins (Matrix of 1s and -1s).
  spins <- randomArrayWith gen uniform (Z :. x :. y) :: IO (Matrix Float)
  let spins' = return $ initialFlip $ use spins
  -- Prepare iteration function.
  let flipSpins' = flipSpins $ constant $ optCrit opts
      iteration' = iteration size gen flipSpins'
  -- Iterate to final state.
  spins'' <- P.iterate iteration' spins' P.!! optIter opts
  -- Write final iteration to file.
  let spins''' = toList $ run $ spins''
  writeSpins spins''' (optPath opts) x y

type SpinRandoms = Acc (Matrix (Int, Float))
type Spins = Acc (Matrix Int)
-- Accelerate cannot create randoms, so an iteration must be done as an IO operation.
iteration :: (Int, Int) -> GenIO -> (SpinRandoms -> Spins) -> IO (Spins) -> IO (Spins)
iteration (x, y) gen flipSpins spins = do
  spins' <- spins
  -- Use randoms to accept or reject spin flips.
  randoms <- randomArrayWith gen uniform (Z :. x :. y) :: IO (Matrix Float)
  let spinsRandoms = zip spins' (use randoms)
  return $ flipSpins spinsRandoms

writeSpins :: [Int] -> FilePath -> Int -> Int -> IO()
writeSpins spins path columns rows = do
  mapM_ (appendFile path) $ map (\(x:xs) -> foldl (\l y -> l ++ ' ':(show y)) (show x) xs ++ "\n") $ chunksOf columns spins


chunksOf :: Int -> [a] -> [[a]]
chunksOf _ [] = []
chunksOf l xs = (take l xs) : (chunksOf l $ drop l xs)
