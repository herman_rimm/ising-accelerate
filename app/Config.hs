{-# LANGUAGE OverloadedStrings #-}

module Config where

import Data.Word
import Options.Applicative

data Options = Options
  { optSize :: (Int, Int)
  , optSeed :: !(Maybe Word32)
  , optIter :: !Int
  , optCrit :: !Float
  , optPath :: !String
  }

size :: Parser (Int, Int)
size = (,) <$> option auto columns <*> option auto rows
  where
    columns =  long "columns"
            <> short 'x'
            <> metavar "INT"
            <> value 256
            <> help "2D lattice column length"
    rows = long "rows"
        <> short 'y'
        <> metavar "INT"
        <> value 256
        <> help "2D lattice row length"

seed :: Parser (Maybe Word32)
seed = optional (option auto seed)
  where
    seed = long "seed"
        <> short 's'
        <> metavar "INT"
        <> help "Random number generator seed"

iterations :: Parser Int
iterations = option auto iter
  where
    iter = long "iterations"
        <> short 'i'
        <> metavar "INT"
        <> value 200
        <> help "Number of iterations before saving"

curiePoint :: Parser Float
curiePoint = option auto curie
  where
    curie =  long "curie_point"
          <> short 'c'
          <> metavar "F32"
          <> value 2.269185
          <> help "Transition point of ferromagnet and paramagnet"

outputPath :: Parser FilePath
outputPath = option str output
  where
    output = long "output_file"
          <> short 'o'
          <> metavar "PATH"
          <> value "out.txt"
          <> help "The file to write the final state to"

options :: ParserInfo Options
options = info (opts <**> helper) desc where
  opts  = Options <$> size <*> seed <*> iterations <*> curiePoint <*> outputPath
  desc  = fullDesc <> headerDoc Nothing
                   <> progDescDoc (Just "\r\nRun the Metropolis algorithm in parallel.")

